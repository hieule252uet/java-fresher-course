import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input the number: ");
        int inputNumber = sc.nextInt();
        GuessNumThread thread1 = new GuessNumThread(inputNumber, "Thread 1");
        GuessNumThread thread2 = new GuessNumThread(inputNumber, "Thread 2");
        thread1.start();
        thread2.start();
    }
}
