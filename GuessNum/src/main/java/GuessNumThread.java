public class GuessNumThread extends Thread {
    private int mInputNumber;
    private int mCount;
    private String mThreadName;

    public GuessNumThread(int mInputNumber, String mThreadName) {
        this.mInputNumber = mInputNumber;
        this.mThreadName = mThreadName;
        this.mCount = 0;
    }

    @Override
    public void run() {
        int guessNumber = 0;
        while (guessNumber != mInputNumber) {
            mCount++;
            guessNumber = (int) (Math.random() * 50 + 1);
            System.out.println(mThreadName + " guesses number: "+ guessNumber);
            try {
                Thread.sleep(500);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(mThreadName + " guessed the input number: "+ mInputNumber + " in "+ mCount + " counting");
    }
}
